HWUI
======

HWUI能够帮助你在Android端快速开发应用的一套主题以及相关自定义控件！


## 联系我

way:
  * [邮箱](mailto:way.ping.li@gmail.com "给我发邮件")
  * [博客](http://blog.csdn.net/way_ping_li "CSDN博客")


## 测试截图
###ListView侧滑删除
![Screenshot 0](http://git.oschina.net/way/HWUI/raw/master/screenshots/0.png "Screenshot 0")

###常用控件
![Screenshot 1](http://git.oschina.net/way/HWUI/raw/master/screenshots/1.png "Screenshot 1")

###ActionBar Tab的使用
![Screenshot 2](http://git.oschina.net/way/HWUI/raw/master/screenshots/2.png "Screenshot 2")

###Preference的使用
![Screenshot 3](http://git.oschina.net/way/HWUI/raw/master/screenshots/3.png "Screenshot 3")

###FootBarMenu的使用
![Screenshot 4](http://git.oschina.net/way/HWUI/raw/master/screenshots/4.png "Screenshot 4")

###Dialog的使用
![Screenshot 5](http://git.oschina.net/way/HWUI/raw/master/screenshots/5.png "Screenshot 5")

###SearchView的使用
![Screenshot 6](http://git.oschina.net/way/HWUI/raw/master/screenshots/6.png "Screenshot 6")

###ActionBar的使用
![Screenshot 7](http://git.oschina.net/way/HWUI/raw/master/screenshots/7.png "Screenshot 7")