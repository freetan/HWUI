
package com.hw.droid.hwcatalog;

import hwdroid.widget.SlidingLeftViewGroup;
import hwdroid.widget.SlidingLeftViewGroup.OnSlideListener;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SlidingLeftItemAdapter extends BaseAdapter implements OnSlideListener {
    private LayoutInflater mLayoutInflater;
    private List<String> mData = null;

    private Context mContext;
    private SlidingLeftViewGroup mSlidingItem;

    public SlidingLeftItemAdapter(Context context, List<String> data) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mData = data;
    }

    public int getCount() {
        return mData.size();
    }

    public Object getItem(int pos) {
        return mData.get(pos);
    }

    public long getItemId(int id) {
        return id;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.multiview, null);
            holder = new ViewHolder();

            holder.mListItem = (SlidingLeftViewGroup) convertView
                    .findViewById(R.id.mymultiViewGroup);
            holder.mLL = (LinearLayout) convertView.findViewById(R.id.item_ll);
            holder.mName = (TextView) convertView.findViewById(R.id.name);
            holder.mDel = (Button) convertView.findViewById(R.id.delete);
            holder.mTop = (Button) convertView.findViewById(R.id.top);
            convertView.setTag(holder);
        }

        holder = (ViewHolder) convertView.getTag();
        setupHolder(holder, convertView, position);

        return convertView;
    }

    private class ViewHolder {
        private int mPos;
        private SlidingLeftViewGroup mListItem;
        private LinearLayout mLL;
        private TextView mName;
        private Button mDel;
        private Button mTop;
    }

    private void setupHolder(ViewHolder holder, View view, int position) {
        holder.mName.setText(mData.get(position));
        holder.mPos = position;
        holder.mLL.setTag(holder);

        holder.mDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Delete onClick", Toast.LENGTH_SHORT).show();
            }
        });
        holder.mTop.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Toast.makeText(mContext, "Top onClick", Toast.LENGTH_SHORT).show();
            }
        });

        holder.mLL.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                StringBuilder sb = new StringBuilder();
                ViewHolder holder = (ViewHolder) view.getTag();
                if (holder != null) {
                    int position = holder.mPos;
                    sb.append("Item ");
                    sb.append(position);
                    sb.append(" onclick");
                }

                Toast.makeText(mContext, sb.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        holder.mListItem.setSlidingListener(this);
    }

    @Override
    public void onSlideBack() {
        mSlidingItem = null;
    }

    @Override
    public void onSlideToLeft(SlidingLeftViewGroup item) {
        mSlidingItem = item;
    }

    @Override
    public void onSlidingStart(SlidingLeftViewGroup item) {
        if (mSlidingItem != null && item != null && mSlidingItem != item) {
            mSlidingItem.MoveBack(false);
        }
    }
}
