package com.hw.droid.hwcatalog.preference;

import hwdroid.app.HWListActivity;
import hwdroid.widget.ItemAdapter;
import hwdroid.widget.item.TextItem;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class PreferenceSample extends HWListActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle2("AUI2.5");

        ItemAdapter adapter = new ItemAdapter(this);
        adapter.add(createTextItem("Preferences from XML", PreferencesFromXml.class));
        adapter.add(createTextItem("Launching Preferences", LaunchingPreferences.class));
        adapter.add(createTextItem("Preference dependencies", PreferenceDependencies.class));
        adapter.add(createTextItem("Default values", DefaultValues.class));
        adapter.add(createTextItem("Preferences from code", PreferencesFromCode.class));
        adapter.add(createTextItem("Advanced preferences", AdvancedPreferences.class));
        adapter.add(createTextItem("Headers", PreferenceWithHeaders.class));
        adapter.add(createTextItem("Switch", SwitchPreference.class));
        setListAdapter(adapter);
        
        getListView().setBackgroundColor(Color.WHITE);
    }
    
    private TextItem createTextItem(String str, Class<?> klass) {
        final TextItem textItem = new TextItem(str);
        textItem.setTag(klass);
        return textItem;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        final TextItem textItem = (TextItem) l.getAdapter().getItem(position);
        Intent intent = new Intent(PreferenceSample.this, (Class<?>) textItem.getTag());
        startActivity(intent);
    }
}
